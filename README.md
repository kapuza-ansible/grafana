# Grafana install role for ubuntu or centos
## Use
```bash
# Cd to example
cd example

# Edit ansible cfg (remote_user)
vim ansible.cfg

# Edit inventory/hosts
vim inventory/hosts

# Run role
ansible-playbook install.yml
```

## Check
Use browser to enter new grafana http://{server_name}:3000
* User: admin
* Passwd: admin
